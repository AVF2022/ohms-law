import sys
from PyQt5.QtWidgets import (QApplication, QWidget, QLabel, QLineEdit, QPushButton)


class App(QWidget):
    def __init__(self):
        super().__init__()

        self.voltage_label = QLabel(self, text="Напряжение")
        self.voltage_edit = QLineEdit(self)

        self.current_label = QLabel(self, text="Ток")
        self.current_edit = QLineEdit(self)

        self.power_label = QLabel(self, text="Мощность")
        self.power_edit = QLineEdit(self)

        self.calculate_button = QPushButton(self, text="Вычислить")
        self.calculate_button.clicked.connect(self.calculate)

        self.voltage_label.move(10, 10)
        self.voltage_edit.move(100, 10)

        self.current_label.move(10, 50)
        self.current_edit.move(100, 50)

        self.power_label.move(10, 90)
        self.power_edit.move(100, 90)

        self.calculate_button.move(100, 130)

    def calculate(self):
        try:
            voltage = float(self.voltage_edit.text())
            current = float(self.current_edit.text())

            if voltage == 0:
                result = "Cannot divide by zero"
            elif current == 0:
                result = "Cannot divide by zero"
            else:
                result = voltage * current

            self.power_edit.setText(str(result))
        except ValueError:
            print("Invalid number entered")


if __name__ == "__main__":
    app = QApplication(sys.argv)

    main_window = App()
    main_window.show()

    sys.exit(app.exec_())
